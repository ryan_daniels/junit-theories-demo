package com.learn.junit;

import java.util.ArrayList;
import java.util.List;
import static java.lang.Math.abs;

import org.junit.experimental.theories.ParameterSignature;
import org.junit.experimental.theories.ParameterSupplier;
import org.junit.experimental.theories.PotentialAssignment;

public class IntRangeSupplier extends ParameterSupplier {

	@Override
	public List<PotentialAssignment> getValueSources(ParameterSignature sig) throws Throwable {
		final IntRange settings = sig.findDeepAnnotation(IntRange.class);
		final List<PotentialAssignment> assignments = new ArrayList<PotentialAssignment>();
		
		final long start = settings.from();
		final long end = settings.to();
		final long inc = start > end ? -1 : 1;
		
		long units = abs(start - end);
		
		for (long i = 0, value = start; i <= units; i++, value += inc) {
			assignments.add(PotentialAssignment.forValue("int", (int) value));
		}
		
		return assignments;
	}

}
