package com.learn.junit;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;

import org.apache.commons.beanutils.BeanUtils;

/**
 * Hibernate validator assertions
 *
 */
public final class ValidatorAsserts {
	
	private static final Validator validator = Validation.buildDefaultValidatorFactory().getValidator();
	
	private ValidatorAsserts() {}
	
	public static void assertIsValid(final Object target, final String propertyName, final Object value) {
		
		try {
			BeanUtils.setProperty(target, propertyName, value);
		} catch (Exception e) {
			new RuntimeException(e);
		}
		
		assertIsValid(target, propertyName);
	}
	
	public static void assertIsValid(final Object target, final String propertyName) {
		
		ValidationResult result = validate(target, propertyName);
		
		if (!result.isValid()) {
			String message = "Excpected object %s to be valid but was invalid:%n%s";
			throw new AssertionError(String.format(message, target.toString(), result.toString()));
		}
	}
	
	public static void assertIsNotValid(final Object target, final String propertyName) {
		
		ValidationResult result = validate(target, propertyName);
		
		if (result.isValid()) {
			String message = "Excpected object %s to be invalid but was valid";
			throw new AssertionError(String.format(message, target.toString()));
		}
		
	}
	
	public static void assertIsNotValid(final Object target, final String propertyName, final Object value) {
		
		try {
			BeanUtils.setProperty(target, propertyName, value);
		} catch (Exception e) {
			new RuntimeException(e);
		}
		
		assertIsNotValid(target, propertyName);
	}
	
	private static ValidationResult validate(final Object target, final String propertyName) {
		return new ValidationResult(target, propertyName);
	}
	
	private static class ValidationResult {
		
		private static final String newline = String.format("%n");
		private Set<ConstraintViolation<Object>> violations;
		
		public ValidationResult(final Object target, final String propertyName) {
			this.violations = validator.validateProperty(target, propertyName);
		}
		
		public boolean isValid() {
			return 0 == this.violations.size();
		}
		
		public String toString() {
			StringBuffer sb = new StringBuffer();
			
			for (ConstraintViolation<Object> v : this.violations) {
				sb.append(v.getMessage());
				sb.append(newline);
			}
			
			return sb.toString();
		}
	}
}
