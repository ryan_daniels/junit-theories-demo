package com.learn.junit;

import static com.learn.junit.ValidatorAsserts.assertIsNotValid;
import static com.learn.junit.ValidatorAsserts.assertIsValid;
import static org.hamcrest.Matchers.both;
import static org.hamcrest.Matchers.either;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThan;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.junit.Assume.assumeThat;

import org.junit.Before;
import org.junit.experimental.theories.DataPoints;
import org.junit.experimental.theories.FromDataPoints;
import org.junit.experimental.theories.Theories;
import org.junit.experimental.theories.Theory;
import org.junit.runner.RunWith;

/**
 * Test demonstrating JUnit Theories. Each test method
 * will run multiple times based on the number of input
 * values.
 * 
 * Custom parameter suppliers are also demonstrated here:
 * @IntRange
 * @BlankString
 * 
 * @author Ryan Daniels
 *
 */
@RunWith(Theories.class)
public class SomeEntityTest {
	
	@DataPoints("validStrings")
	public static final String[] validStrings = {"A", " B ", "ABC", "123"};
	
	SomeEntity entity;
	
	@Before
	public void setUp() {
		entity = new SomeEntity();
	}
	
	@Theory
	public void numberValidInRange(@IntRange(from=-5, to=5) final int value) {
		assertIsValid(entity, "number", value);
	}
	
	/*
	 * Method will be run for the full range of values from -1000 to 1000.
	 * 
	 * Assume will "abort" the invocation of the test method for the values
	 * that fail the assumption. Those will be considered complete and will
	 * not be counted as an error.
	 * 
	 * assertIsNotValid() will set the property "number" to the value and run
	 * the Hibernate validator to check for validity and the test will fail
	 * if the validations do not fail.
	 */
	@Theory
	public void numberInvalidOutOfRange(@IntRange(from=-1000, to=1000) final int value) {
		
		assumeThat(value, either(lessThan(-5)).or(greaterThan(5)));
		
		assertIsNotValid(entity, "number", value);
	}
	
	@Theory
	public void textInvalidForBlank(@BlankString final String value) {
		assertIsNotValid(entity, "text", value);
	}
	
	@Theory
	public void textValidForNonBlank(@FromDataPoints("validStrings") final String value) {
		assertIsValid(entity, "text", value);
	}
}
