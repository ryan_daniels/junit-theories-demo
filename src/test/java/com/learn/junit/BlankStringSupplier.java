package com.learn.junit;

import java.util.ArrayList;
import java.util.List;

import org.junit.experimental.theories.ParameterSignature;
import org.junit.experimental.theories.ParameterSupplier;
import org.junit.experimental.theories.PotentialAssignment;

public class BlankStringSupplier extends ParameterSupplier {

	@Override
	public List<PotentialAssignment> getValueSources(ParameterSignature sig) throws Throwable {
		List<PotentialAssignment> values = new ArrayList<PotentialAssignment>();
		values.add(PotentialAssignment.forValue("blank", null));
		values.add(PotentialAssignment.forValue("blank", ""));
		values.add(PotentialAssignment.forValue("blank", " "));
		values.add(PotentialAssignment.forValue("blank", "          "));
		return values;
	}

}
